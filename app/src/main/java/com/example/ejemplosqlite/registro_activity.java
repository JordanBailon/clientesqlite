package com.example.ejemplosqlite;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.service.autofill.TextValueSanitizer;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ejemplosqlite.utilidades.utilidades;

public class registro_activity extends AppCompatActivity {
    TextView IMEI, GAMA,MARCA,MODELO,COMPA;
    Button btnRegistrar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_activity);



        IMEI = (TextView) findViewById(R.id.editTextId);
        GAMA= (TextView) findViewById(R.id.editTextNombre);
        MARCA = (TextView) findViewById(R.id.editTextTelefono);
        MODELO = (TextView) findViewById(R.id.editText1);

       COMPA = (TextView) findViewById(R.id.editT2);

        btnRegistrar = (Button)findViewById(R.id.btnRegistrarUsuario);
        setupActionBAr();
        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registrarUsuarios();
            }
        });
    }



    private void registrarUsuarios() {
        ConexionSQLiteHelper conn= new ConexionSQLiteHelper(this, "bd_usuarios",null,1);

        SQLiteDatabase db=conn.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(utilidades.CAMPO_IMEI, IMEI.getText().toString());
        values.put(utilidades.CAMPO_Gama, GAMA.getText().toString());
        values.put(utilidades.CAMPO_Marca, MARCA.getText().toString());
        values.put(utilidades.CAMPO_Modelo, MODELO.getText().toString());
        values.put(utilidades.CAMPO_Compa, COMPA.getText().toString());

        Long idResultante=db.insert(utilidades.TABLA_Tegnologia,utilidades.CAMPO_IMEI,values);

        Toast.makeText(getApplicationContext(),"Id Registro: "+idResultante,Toast.LENGTH_SHORT).show();
        db.close();

    }
    private void setupActionBAr() {
        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null){
            actionBar.setDisplayHomeAsUpEnabled(true);

        }
    }
}
